package com.test.basket.service;

import com.test.basket.domain.Basket;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static com.test.basket.TestHelper.*;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.HALF_UP;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class BasketServiceImplTest {

    private BasketServiceImpl basketService;

    @Before
    public void setup() {
        basketService = new BasketServiceImpl();
    }

    @Test
    public void shouldCreateBasket() throws Exception {

        //Given
        Basket basket = basketService.createBasket();

        //Then
        assertNotNull(basket);
    }

    @Test
    public void shouldAddButterToBasket() throws Exception {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createButter());

        //Then
        assertNotNull(basket);
        assertThat(basket.getItems().size(), is(1));
        assertThat(basket.getTotal(), is(valueOf(0.80)));
    }

    @Test
    public void shouldRemoveMilkFromBasketAfterAddingTwo() {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createMilk());
        basketService.addItemToBasket(basket, createBread());

        basketService.removeItemFromBasket(basket, createBread().getId());

        //Then
        assertNotNull(basket);
        assertThat(basket.getItems().size(), is(1));
        assertThat(basket.getTotal(), is(valueOf(1.15)));
    }

    @Test
    public void shouldRecalculateTotalWithTwoButters() {

        //Given
        Basket basket = new Basket();
        basket.getItems().add(createButter());

        basket = basketService.recalculateTotal(basket);

        //Then
        assertThat(basket.getTotal(), is(valueOf(0.80)));

        //Given
        basket.getItems().add(createButter());

        basket = basketService.recalculateTotal(basket);

        //Then
        assertThat(basket.getTotal(), is(valueOf(1.60)));

        //Given
        basket.getItems().remove(1);

        basket = basketService.recalculateTotal(basket);

        //Then
        assertThat(basket.getTotal(), is(valueOf(0.80)));

        //Given
        basket.getItems().remove(0);

        basket = basketService.recalculateTotal(basket);

        //Then
        assertThat(basket.getTotal(), is(ZERO));
    }

    @Test
    public void scenarioOne() {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createMilk());
        basketService.addItemToBasket(basket, createBread());
        basketService.addItemToBasket(basket, createButter());

        //Then
        assertThat(basket.getTotal(), is(valueOf(2.95)));

    }

    @Test
    public void scenarioTwo() {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createBread());
        basketService.addItemToBasket(basket, createBread());
        basketService.addItemToBasket(basket, createButter());
        basketService.addItemToBasket(basket, createButter());

        //Then
        assertThat(basket.getTotal(), is(valueOf(3.10)));
    }

    @Test
    public void scenarioThree() {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());

        //Then
        assertThat(basket.getTotal(), is(valueOf(3.45)));
    }

    @Test
    public void scenarioFour() {

        //Given
        Basket basket = basketService.addItemToBasket(new Basket(), createButter());
        basketService.addItemToBasket(basket, createButter());
        basketService.addItemToBasket(basket, createBread());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());
        basketService.addItemToBasket(basket, createMilk());


        //Then
        //This is to make sure comparison has the same number of trailing zeros
        BigDecimal testValue = valueOf(9.00).setScale(2, HALF_UP);
        assertThat(basket.getTotal(), is(testValue));
    }

    @Test
    public void shouldRecalculateDiscount() {

        //Given
        Basket basket = new Basket();
        basket.getItems().add(createButter());
        basket.getItems().add(createButter());
        basket.getItems().add(createBread());
        basket.getItems().add(createBread());
        basket.setTotal(valueOf(3.6));

        basketService.recalculateDiscount(basket);

        //Then
        assertThat(basket.getTotal(), is(valueOf(3.10)));
    }

}