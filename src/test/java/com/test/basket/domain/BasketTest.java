package com.test.basket.domain;

import org.junit.Test;

import static com.test.basket.TestHelper.createButter;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BasketTest {

    @Test
    public void addItem() throws Exception {
        //Given

        Basket basket = new Basket();
        basket = basket.addItem(createButter());

        //Then
        assertThat(basket.getItems().size(), is(1));
    }

}