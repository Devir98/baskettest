package com.test.basket;

import com.test.basket.domain.Item;

import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.valueOf;

public class TestHelper {

    public static Item createButter() {
        return Item.builder().id("1").cost(valueOf(0.80)).name("Butter").quantity(1).build();
    }

    public static Item createMilk() {
        return Item.builder().id("2").cost(valueOf(1.15)).name("Milk").quantity(1).build();
    }

    public static Item createBread() {
        return Item.builder().id("3").cost(ONE).name("Bread").quantity(1).build();
    }

}
