package com.test.basket.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;

@Builder
@Data
public class Decision {

    private String itemId;
    private BigDecimal discount = ZERO;
}
