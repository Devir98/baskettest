package com.test.basket.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Condition {

    private String itemId;
    private int quantity;
}
