package com.test.basket.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.math.BigDecimal.ZERO;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Basket {

    private String id;
    private List<Item> items = new ArrayList<>();
    private BigDecimal total = ZERO;

    public Basket addItem(Item item){
        items.add(item);

        return this;
    }
}
