package com.test.basket.domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DiscountRule {

    private String id;
    private Condition condition;
    private Decision decision;

}
