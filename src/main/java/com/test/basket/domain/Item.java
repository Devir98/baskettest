package com.test.basket.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class Item {

    private String id;
    private String name;
    private int quantity;
    private BigDecimal cost;
}
