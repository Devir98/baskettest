package com.test.basket.discount;

import com.test.basket.domain.Condition;
import com.test.basket.domain.Decision;
import com.test.basket.domain.DiscountRule;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.math.BigDecimal.valueOf;

public class Rules {

    public static List<DiscountRule> getRules() {
        return newArrayList(createButterDiscount(), createMilkDiscount());
    }

    private static DiscountRule createButterDiscount() {
        return DiscountRule.builder().id("1").condition(
                Condition.builder()
                        .itemId("1")
                        .quantity(2)
                        .build())
                .decision(Decision.builder()
                        .itemId("3")
                        .discount(valueOf(0.5))
                        .build())
                .build();
    }

    private static DiscountRule createMilkDiscount() {
        return DiscountRule.builder().id("1").condition(
                Condition.builder()
                        .itemId("2")
                        .quantity(3)
                        .build())
                .decision(Decision.builder()
                        .itemId("2")
                        .discount(valueOf(1.15))
                        .build())
                .build();
    }
}
