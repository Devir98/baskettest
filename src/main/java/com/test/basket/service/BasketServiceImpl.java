package com.test.basket.service;

import com.google.common.annotations.VisibleForTesting;
import com.test.basket.domain.Basket;
import com.test.basket.domain.DiscountRule;
import com.test.basket.domain.Item;

import java.math.BigDecimal;
import java.util.List;

import static com.test.basket.discount.Rules.getRules;
import static java.util.stream.Collectors.toList;

public class BasketServiceImpl implements BasketService {

    private final List<DiscountRule> discountRules = getRules();

    public Basket createBasket() {
        return new Basket();
    }

    public Basket addItemToBasket(Basket basket, Item item) {
        basket.getItems().add(item);

        return recalculateTotal(basket);
    }

    public Basket removeItemFromBasket(Basket basket, String itemId) {

        basket.setItems(basket.getItems().stream().filter(item -> !item.getId().equals(itemId)).collect(toList()));

        return recalculateTotal(basket);
    }

    @VisibleForTesting
    Basket recalculateTotal(Basket basket) {

        BigDecimal newTotal = BigDecimal.ZERO;

        for (Item item : basket.getItems()) {
            newTotal = newTotal.add(item.getCost().multiply(BigDecimal.valueOf(item.getQuantity())));
        }
        basket.setTotal(newTotal);

        return recalculateDiscount(basket);
    }

    @VisibleForTesting
    Basket recalculateDiscount(Basket basket) {

        BigDecimal currentTotal = basket.getTotal();

        for (DiscountRule discountRule : discountRules) {

            List<Item> matching = basket.getItems().stream().filter(item -> item.getId().equals(discountRule.getCondition().getItemId())).collect(toList());

            if (!matching.isEmpty()) {
                int noOfDiscount = (int) Math.floor(matching.size() / discountRule.getCondition().getQuantity());

                List<Item> eligible = basket.getItems().stream().filter(item -> item.getId().equals(discountRule.getDecision().getItemId())).collect(toList());

                if (!eligible.isEmpty()) {
                    currentTotal = currentTotal.subtract(
                            discountRule.getDecision().getDiscount().multiply(
                                    BigDecimal.valueOf(Math.min(noOfDiscount, eligible.size()))));
                }
            }

        }

        basket.setTotal(currentTotal);

        return basket;
    }
}
