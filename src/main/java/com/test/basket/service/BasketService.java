package com.test.basket.service;

import com.test.basket.domain.Basket;
import com.test.basket.domain.Item;

@SuppressWarnings("unused")
interface BasketService {

    Basket createBasket();

    Basket addItemToBasket(Basket basket, Item item);

    Basket removeItemFromBasket(Basket basket, String itemId);
}
